import csv
import random
import sys

N = int(sys.argv[1])
print(N)

mu_mass = 1.3 * 1e25
sigma_mass = mu_mass / 10

mu_x = 1.5 * 1e9
mu_y = 1.1 * 1e9
mu_z = 7.3 * 1e7

sigma_x = mu_x / 10
sigma_y = mu_y / 10
sigma_z = mu_z / 10

mu_vx = 2.2 * 1e4
mu_vy = 2.1 * 1e4
mu_vz = 1.4 * 1e3

sigma_vx = mu_vx / 10
sigma_vy = mu_vy / 10
sigma_vz = mu_vz / 10

def generate_row():
    row = list()

    mass = random.gauss(mu_mass, sigma_mass)
    x = random.gauss(mu_x, sigma_x)
    y = random.gauss(mu_y, sigma_y)
    z = random.gauss(mu_z, sigma_z)

    vx = random.gauss(mu_vx, sigma_vx)
    vy = random.gauss(mu_vy, sigma_vy)
    vz = random.gauss(mu_vz, sigma_vz)

    row.append(mass)

    row.append(x)
    row.append(y)
    row.append(z)

    row.append(vx)
    row.append(vy)
    row.append(vz)
    return row

with open("data.csv", "w") as csv_file:
    writer = csv.writer(csv_file, delimiter=',', quoting=csv.QUOTE_MINIMAL)

    for i in range(N):
        writer.writerow(generate_row())