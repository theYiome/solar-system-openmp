import csv
import sys
import numpy as np

file_1 = sys.argv[1]
file_2 = sys.argv[2]

print(file_1, file_2)

data_1 = list()
data_2 = list()

with open(file_1, "r") as csv_file_1:
    reader_1 = csv.reader(csv_file_1, delimiter=',', quoting=csv.QUOTE_MINIMAL)
    for row in reader_1:
        data_1.append(row)

with open(file_2, "r") as csv_file_2:
    reader_2 = csv.reader(csv_file_2, delimiter=',', quoting=csv.QUOTE_MINIMAL)
    for row in reader_2:
        data_2.append(row)


# index = 1
# d1 = [float(x) for x in data_1[0]]
# d2 = [float(x) for x in data_1[0]]
# r1 = np.array(d1)
# r2 = np.array(d2)

# print(r1.dtype)
# print(r2)

N = len(data_1)
error_sum = 0
for index in range(N):
    d1 = [float(x) for x in data_1[index]]
    d2 = [float(x) for x in data_2[index]]

    r1 = np.array(d1)
    r2 = np.array(d2)

    s = np.sum(r1 - r2)
    error_sum += np.abs(s)

print(error_sum / N)
