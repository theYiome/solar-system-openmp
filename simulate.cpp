#include <vector>
#include <omp.h>
#include <mpi.h>

#include "simulate.h"
#include "Vector.h"
#include "Body.h"

namespace simulate
{

    void single_cpu(std::vector<Body> &objects, const double dt)
    {
        const int N = objects.size();
        const double G = 0.0000000000667;

        #pragma omp parallel for
        for (int i = 0; i < N; ++i)
        {
            Body &b1 = objects[i];

            for (int j = 0; j < N; ++j)
            {
                if (i == j)
                    continue;

                Body &b2 = objects[j];

                const vec3 diff = b2.position - b1.position;
                const double r = diff.length();
                double acceleration = (G * b2.mass) / (r * r);

                // simple update velocity
                b1.velocity += diff.normalized() * acceleration * dt;
            }
        }

        #pragma omp parallel for
        for (int i = 0; i < N; ++i)
        {
            Body &b1 = objects[i];
            b1.position += b1.velocity * dt;
        }
    }

    void multi_cpu(std::vector<Body> &objects, const double dt, const int rank, const int size)
    {
        const int N = objects.size();
        const double G = 0.0000000000667;

        const int start = simulate::start(N, rank, size);
        const int end = simulate::end(N, rank, size);

        #pragma omp parallel for
        for (int i = start; i < end; ++i)
        {
            Body &b1 = objects[i];

            for (int j = 0; j < N; ++j)
            {
                if (i == j)
                    continue;

                Body &b2 = objects[j];

                const vec3 diff = b2.position - b1.position;
                const double r = diff.length();
                double acceleration = (G * b2.mass) / (r * r);

                // simple update velocity
                b1.velocity += diff.normalized() * acceleration * dt;
            }
        }

        #pragma omp parallel for
        for (int i = start; i < end; ++i)
        {
            Body &b1 = objects[i];
            b1.position += b1.velocity * dt;
        }
    }

    void synchronize(std::vector<Body> &objects, MPI_Datatype datatype, const int rank, const int size)
    {
        const unsigned long N = objects.size();

        for (unsigned current_rank = 0; current_rank < size; ++current_rank)
        {

            const int current_start = simulate::start(N, current_rank, size);
            const int current_end = simulate::end(N, current_rank, size);
            const unsigned long data_size = current_end - current_start;

            MPI_Bcast(objects.data() + current_start, data_size, datatype, current_rank, MPI_COMM_WORLD);
        }
    }

    int start(int n, int rank, int size)
    {
        const int chunk_size = n / size;
        return rank * chunk_size;
    }

    int end(int n, int rank, int size)
    {
        if (rank + 1 == size)
            return n;

        const int chunk_size = n / size;
        return (rank + 1) * chunk_size;
    }
}