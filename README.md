# solar-system-openmp

## How to build
make

## How to run
make run

## Requirements
- mpic++
- -lm
- -fopenmp