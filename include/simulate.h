#pragma once
#include <vector>
#include "Body.h"

namespace simulate
{
    void single_cpu(std::vector<Body> &objects, const double dt);

    void multi_cpu(std::vector<Body> &objects, const double dt, const int rank, const int size);

    void synchronize(std::vector<Body> &objects, MPI_Datatype datatype, const int rank, const int size);

    int start(int n, int rank, int size);

    int end(int n, int rank, int size);
}