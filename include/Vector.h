#pragma once
#include <iostream>
#include <math.h>

template <typename T>
struct Vector3
{
    T x;
    T y;
    T z;

    Vector3() = default;
    Vector3(const T &_x, const T &_y, const T &_z)
    {
        x = _x;
        y = _y;
        z = _z;
    }

    double length() const
    {
        return sqrt(x * x + y * y + z * z);
    }

    Vector3 normalized() const
    {
        return *this / length();
    }

    double distance(const Vector3 &b) const
    {
        double dx = b.x - x;
        double dy = b.y - y;
        double dz = b.z - z;
        return sqrt(dx * dx + dy * dy + dz * dz);
    }

    Vector3 operator+(const Vector3 &b) const
    {
        Vector3 out;
        out.x = x + b.x;
        out.y = y + b.y;
        out.z = z + b.z;
        return out;
    }

    Vector3 &operator+=(const Vector3 &b)
    {
        x += b.x;
        y += b.y;
        z += b.z;
        return *this;
    }

    Vector3 operator-(const Vector3 &b) const
    {
        Vector3 out;
        out.x = x - b.x;
        out.y = y - b.y;
        out.z = z - b.z;
        return out;
    }

    Vector3 &operator-=(const Vector3 &b)
    {
        x -= b.x;
        y -= b.y;
        z -= b.z;
        return *this;
    }

    Vector3 operator*(const double &b) const
    {
        Vector3 out;
        out.x = x * b;
        out.y = y * b;
        out.z = z * b;
        return out;
    }

    Vector3 &operator*=(const double &b)
    {
        x *= b;
        y *= b;
        z *= b;
        return *this;
    }

    Vector3 operator/(const double &b) const
    {
        Vector3 out;
        out.x = x / b;
        out.y = y / b;
        out.z = z / b;
        return out;
    }

    Vector3 &operator/=(const double &b)
    {
        x /= b;
        y /= b;
        z /= b;
        return *this;
    }
};

template <typename T>
std::ostream &operator<<(std::ostream &os, const Vector3<T> &vec)
{
    os << "[" << vec.x << ", " << vec.y << ", " << vec.z << "]";
    return os;
}

typedef Vector3<float> Vector3f;
typedef Vector3<double> Vector3d;

typedef Vector3<double> vec3;