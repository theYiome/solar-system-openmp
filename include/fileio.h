#pragma once
#include <cstdlib>
#include <cstdio>
#include <vector>

#include "Vector.h"
#include "Body.h"

namespace fileio
{
    std::vector<Body> load(const char *file_name)
    {
        FILE *fp = fopen(file_name, "r");
        char *line = NULL;
        size_t len = 0;
        ssize_t read;

        std::vector<Body> objects;

        double m, px, py, pz, vx, vy, vz;
        vec3 pos;
        vec3 vel;
        Body body;

        while ((read = getline(&line, &len, fp)) != -1)
        {
            sscanf(line, "%lf,%lf,%lf,%lf,%lf,%lf,%lf\n", &m, &px, &py, &pz, &vx, &vy, &vz);
            pos = vec3(px, py, pz);
            vel = vec3(vx, vy, vz);

            objects.push_back(Body(m, pos, vel));
        }

        fclose(fp);

        if (line)
            free(line);

        return objects;
    }

    void save(const char *file_name, const std::vector<Body> &data)
    {
        FILE *fp = fopen(file_name, "w");

        double m, px, py, pz, vx, vy, vz;

        for (const Body &b : data)
        {
            m = b.mass;

            px = b.position.x;
            py = b.position.y;
            pz = b.position.z;

            vx = b.velocity.x;
            vy = b.velocity.y;
            vz = b.velocity.z;
            fprintf(fp, "%lf,%lf,%lf,%lf,%lf,%lf,%lf\n", m, px, py, pz, vx, vy, vz);
        }

        fclose(fp);
    }
}