#pragma once
#include "Vector.h"

struct Body
{
    double mass;
    vec3 position;
    vec3 velocity;

    Body() = default;

    Body(const double &_mass, const vec3 &_position, const vec3 &_velocity)
    {
        mass = _mass;
        position = _position;
        velocity = _velocity;
    }
};

static std::ostream &operator<<(std::ostream &os, const Body &b)
{
    os << "(" << b.mass << ", " << b.position << ", " << b.velocity << ")";
    return os;
}