.PHONY = all clean

CC = mpic++

LINKER_FLAGS = -lm -fopenmp
COMPILE_FLAGS = -fopenmp -Ofast
INCLUDE = -Iinclude

BUILD_DIR = build

SRCS := *.cpp
BINS := *.o

all: init main

init:
	mkdir -p ${BUILD_DIR}

clean: init
	@echo "Cleaning up:"
	rm -rvf ${BUILD_DIR}/*
	rm -rvf ${BINS}

clear: clean

main: ${DIR}/${BINS}
	@echo "Creating executable:"
	cd ${BUILD_DIR} && ${CC} ${LINKER_FLAGS} ${BINS}

${DIR}/${BINS}: ${SRCS}
	@echo "Creating objects:"
	${CC} ${COMPILE_FLAGS} ${INCLUDE} -c ${SRCS}
	mv *.o ${BUILD_DIR}

run:
	mpiexec -n 1 ${BUILD_DIR}/a.out ${argv1} ${argv2}

multirun:
	mpiexec -n 12 -f nodes ${BUILD_DIR}/a.out ${arg1} ${argv2}

test:
	valgrind -s ${BUILD_DIR}/a.out

full: clean main run

