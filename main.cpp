#include <stdio.h>
#include <mpi.h>

#include "fileio.h"
#include "simulate.h"

std::string output_file(int iterations, double dt, int size) {
    std::string out = "output_mpi_i" + std::to_string(iterations) + "_dt" + std::to_string(dt) + "_n" + std::to_string(size) + ".csv";
    return out;
}

void synchronize_objects() {
    MPI_Datatype datatype_body;
    MPI_Type_contiguous(7, MPI_DOUBLE, &datatype_body);
    MPI_Type_commit(&datatype_body);
}

int main(int argc, char *argv[])
{
    const int root = 0;
    int rank, size;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    unsigned iterations = 10;
    double dt = 60.f;

    std::vector<Body> objects;
    unsigned long N;

    if (rank == root)
    {
        // get args
        if (argc > 1)
            iterations = atoi(argv[1]);
        if (argc > 2)
            dt = atof(argv[2]);

        printf("iterations=%u, dt=%lf\n", iterations, dt);

        // load data
        objects = fileio::load("data.csv");
        N = objects.size();
    }

    // synchronize args
    {
        MPI_Bcast(&iterations, 1, MPI_UNSIGNED, root, MPI_COMM_WORLD);
        MPI_Bcast(&dt, 1, MPI_DOUBLE, root, MPI_COMM_WORLD);
        MPI_Bcast(&N, 1, MPI_UNSIGNED_LONG, root, MPI_COMM_WORLD);
    }

    // custom datatype
    MPI_Datatype datatype_body;
    MPI_Type_contiguous(7, MPI_DOUBLE, &datatype_body);
    MPI_Type_commit(&datatype_body);

    // synchronize objects
    {
        if(rank != root)
            objects.resize(N);

        MPI_Bcast(objects.data(), N, datatype_body, root, MPI_COMM_WORLD);
    }

    // simulation
    for (unsigned i = 0; i < iterations; ++i)
    {
        if (rank == root)
            printf("current_iteration=%u\n", i);

        simulate::multi_cpu(objects, dt, rank, size);
        simulate::synchronize(objects, datatype_body, rank, size);
    }

    // save data
    if (rank == root)
        fileio::save(output_file(iterations, dt, size).c_str(), objects);

    MPI_Type_free(&datatype_body);
    MPI_Finalize();
    return 0;
}